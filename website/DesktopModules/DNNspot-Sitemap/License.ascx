﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="License.ascx.cs" Inherits="DNNspot.Sitemap.License" %>
<%@ Register TagPrefix="dnnspot" Assembly="DNNspot.Licensing.ClientCommon" Namespace="DNNspot.Licensing.Client.WebControls" %>

<div>
    <dnnspot:ModuleLicenseAdminControl id="licenseControl" runat="server"></dnnspot:ModuleLicenseAdminControl>
</div>